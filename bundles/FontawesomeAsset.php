<?php

namespace adminui\bundles;

use yii\web\AssetBundle;

/**
 * Class FontawesomeAsset
 *
 * @author Vitaly V.Shinkarenko <vitaly@shinkarenko.net>
 * @package adminui\bundles
 */
class FontawesomeAsset extends AssetBundle
{
    public $sourcePath = '@bower/fontawesome/';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.min.css',
    ];

}
