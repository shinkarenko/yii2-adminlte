<?php

namespace adminui\bundles;

use Yii;
use yii\helpers\Url;
use yii\web\AssetBundle;
use yii\web\AssetManager;
use yii\web\UrlManager;
use yii\web\View;
use yii\web\YiiAsset;

/**
 * Class HeadAsset
 *
 * @author Vitaly V.Shinkarenko <vitaly@shinkarenko.net>
 * @package adminui\bundles
 */
class HeadAsset extends AssetBundle
{

    public $js = [
        '@bower/html5shiv/dist/html5shiv.js',
        '@bower/respond/dest/respond.min.js',
    ];

    public $jsOptions = [
        'condition' => 'lt IE 9',
        'position' => View::POS_HEAD,
    ];


    public function publish($am)
    {
        $this->baseUrl = Url::home(true);
        foreach ($this->js as $index => $js) {
            $am->publish($js, $this->publishOptions);
            $baseUrl = $am->getPublishedUrl($js);
            $this->js[$index] = $baseUrl;
        }

        foreach ($this->css as $index => $css) {
            $am->publish($css);
            $baseUrl = $am->getPublishedUrl($css, $this->publishOptions);
            $this->css[$index] = $baseUrl;
        }

        parent::publish($am);
    }
}