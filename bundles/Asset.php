<?php

namespace adminui\bundles;

use yii\web\AssetBundle;

/**
 * Class Asset
 *
 * @author Vitaly V.Shinkarenko <vitaly@shinkarenko.net>
 * @package adminui\bundles
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@vendor/shinkarenko/yii2-adminlte/assets';
    public $baseUrl = '@web';
    public $css = [
        'css/AdminLTE.css',
    ];

    public $js = [
        'js/app.js',
        'js/dashboard.js',

    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\bootstrap\BootstrapThemeAsset',
        'adminui\bundles\HeadAsset',
        'adminui\bundles\IoniconsAsset',
        'adminui\bundles\FontawesomeAsset',
        'adminui\bundles\DataHrefAsset',

    ];
}
