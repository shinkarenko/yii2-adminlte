<?php

namespace adminui\bundles;

use yii\web\AssetBundle;

/**
 * Class IoniconsAsset
 *
 * @author Vitaly V.Shinkarenko <vitaly@shinkarenko.net>
 * @package adminui\bundles
 */
class IoniconsAsset extends AssetBundle
{
    public $sourcePath = '@bower/ionicons/';
    public $baseUrl = '@web';
    public $css = [
        'css/ionicons.min.css',
    ];

}
