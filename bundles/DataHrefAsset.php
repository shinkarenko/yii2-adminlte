<?php

namespace adminui\bundles;

use yii\web\AssetBundle;

/**
 * Class DataHrefAsset
 *
 * @author Vitaly V.Shinkarenko <vitaly@shinkarenko.net>
 * @package adminui\bundles
 */
class DataHrefAsset extends AssetBundle
{
    public $sourcePath = '@vendor/shinkarenko/yii2-adminlte/assets';
    public $baseUrl = '@web';
    public $js = [
        'js/datahref.jquery.js',
    ];

}
